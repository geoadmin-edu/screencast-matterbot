# coding: utf-8
import os
import re
from mattermost_bot.bot import listen_to, respond_to

BASE_URL = 'https://sigmageosistemas.com.br/media/upload'


@listen_to('uatt?', re.IGNORECASE)
def uatt(message):
    dir(message)
    message.send(os.path.join(BASE_URL, 'uatt.jpeg'))


@listen_to('el-patron', re.IGNORECASE)
def patron(message):
    message.send(os.path.join(BASE_URL, 'el-patron.jpeg'))


@listen_to('champs', re.IGNORECASE)
def champs(message):
    message.send(os.path.join(BASE_URL, 'champs.png'))


@listen_to('oreia', re.IGNORECASE)
def oreia(message):
    message.send(os.path.join(BASE_URL, 'oreia.jpg'))


@listen_to('asno', re.IGNORECASE)
def asno(message):
    message.send(os.path.join(BASE_URL, 'jackass.jpg'))


@respond_to('matematica (\d+) (\d+)', re.IGNORECASE)
def math(message, x, y):
    x = int(x)
    y = int(y)
    z = x + y
    message.reply('o resultado da soma de {0} e {1} eh {2}'.format(x, y, z))


@listen_to('ola mundo cruel. (.+)')
def mundo_cruel(message, params):
    message.send(params)
